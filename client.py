import socket
import threading
import time
from queue import Queue
from enum import Enum

class WorkerState(Enum):
    Juggling = 0
    StartExfil = 1
    RunningExfil = 2


sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.bind(("", 1337))

bufsize = 1024
header = b"BEGIN_JUGGLE_DATA"  # just some magic bytes
footer = b"END_JUGGLE_DATA"
data_out: Queue[bytes] = Queue()
cmd_q: Queue[None]


def echo_worker():
    """ Data loop - send and receive udp data. forever. """
    state = WorkerState.Juggling
    while True:
        if state == WorkerState.Juggling and not cmd_q.empty():
            # someone asked for exfiltration
            # start waiting for beginning header
            cmd_q.get()
            state = WorkerState.StartExfil

        # receive bufsize bytes, measure time
        start = time.time()
        data = sock.recv(bufsize)
        end = time.time()
        diff = end - start
        # if diff > 0.2:
        #     print(f"waited for {diff:02f} seconds")

        # found beginning of stream, start exfiltrating chunks in order
        if state == WorkerState.StartExfil and data.startswith(header):
            state = WorkerState.RunningExfil

        # put exfil data in outgoing queue
        if state == WorkerState.RunningExfil:
            data_out.put(data)

        # end reached. stop exfiltration
        if state == WorkerState.RunningExfil and data.endswith(footer):
            state = WorkerState.Juggling

        # send data back
        sock.sendto(data, ("127.0.0.1", 1337))


def insert_testfile():
    # add data to the loop
    with open("testfile", "rb") as f:
        data = f.read()

    # prepend header
    data = header + data

    # split into evenly-sized chunks
    chunks = [
        data[chunkno*bufsize : (chunkno+1)*bufsize]
        for chunkno in range((len(data) // bufsize) + 1)
    ]
    # pad last chunk with zeroes
    if (l := len(chunks[-1])) < bufsize:
        chunks[-1] += bytes(bufsize - l)
    assert all(len(chunk) == bufsize for chunk in chunks)

    # send into loop
    for chunk in chunks:
        sock.sendto(chunk, ("127.0.0.1", 1337))

    print("sent the test file")


if __name__ == "__main__":
    # start localhost echo worker
    t = threading.Thread(target=echo_worker)
    t.start()

    insert_testfile()

    t.join()
