#!/usr/bin/env bash

# add 1s of delay on localhost
echo "Setting artificial latency"
tc qdisc add dev lo root handle 1:0 netem delay 1000msec

echo "Ping test"
ping -c 3 localhost

echo "Starting python client"
python3 /client.py
