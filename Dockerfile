FROM debian:bullseye

RUN apt update && apt install -y iproute2 inetutils-ping python3
COPY ./entrypoint.sh /entrypoint.sh
COPY ./client.py /client.py
COPY ./testfile /testfile

ENTRYPOINT /entrypoint.sh
