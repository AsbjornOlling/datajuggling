# Data-on-internet (working title)

This whole thing is super WIP, so this README is basically a notes file.

## Purpose

The idea is to store data as much data as possible *in transit*.
That is, as packets travelling between hosts on the internet.

It would be cool to be able to store more data in-transit, than the machine has available locally (disk or RAM).
For this reason, we can't use protocols that rely on retransmissions for reliability.

That's why we use UDP instead of TCP.
This also means that we need to implement some kind of error correcting codes, if we need it to be reliable.

Also congestion could become an issue. I'm not entirely sure what happens there.
Will we just drop a bunch of UDP packets if 


## Finding Connections

Good connections will have high latency, and high bandwidth.

A quick [ping test to AWS datacenters](https://www.cloudping.cloud/aws)
from my home connection shows about 1s of latency to the furthest datacenter.
So just getting two far apart VPSes could probably give decent latency.

We could try to (ab)use some of the free proxy services on the internet.
Chaining a couple of those could give high latency.

TOR could probably be give some very high latencies- alas it only supports TCP.


## Development

Written in python for now. May be worth rewriting in Rust or C, to eek out more performance.

It's useful to emulate latency, so we can run this thing locally.
Linux has something called `netem` which will do this.
The Dockerfile in this repo is set up to do just that. Run `make` to build and run it.

It would be nice to have some way to check how much data is in the kernel network buffers.
Maybe this can be done in /proc or something? idk


## Measuring capacity

We need to measure the capacity between servers somehow.

I guess stuff starts getting congested and piles up somewhere, if we try to juggle too much data.
But where does it get stuck?
I'm assuming that the UDP packets will just get dropped if they get stuck at an internet router.
If the packets pile up at the end nodes, we will probably see more data in the network buffer.
